<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClanCurrentWarRequest;
use App\Http\Requests\ClanSearchRequest;
use App\Http\Requests\ClanWarlogRequest;
use App\Http\Requests\GetClanMembersRequest;
use App\Http\Requests\GetLocationsRequest;
use App\Http\Requests\GetLocationTopClans;
use App\Http\Requests\GetLocationTopPlayers;
use App\Http\Requests\GetPlayerByTagRequest;
use App\Http\Requests\SearchClanByTagRequest;
use App\Interfaces\SearchInterface;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class SearchController extends Controller implements SearchInterface
{
    public $guzzle;

    public function __construct()
    {
        $this->guzzle = new Client(['headers' => ['Authorization' => config('clash_api_config.api_key')]]);
    }

    /**
     * desc:will return clans by the search params provided for it .
     * @param ClanSearchRequest $request
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function clans(ClanSearchRequest $request)
    {
        $params = json_decode($request->params, true);
        $params = ['query' => $params];
        $uri=config('clash_api_config.default_url') . '/clans';
        return $this->getResponse($uri,$params);
    }

    /**
     * desc:will return the clan if there is any using the clan's tag id .
     * @param Request $request
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function clanByTag(SearchClanByTagRequest $request)
    {
        $tagId = str_replace('#','',$request->tagId);
        $uri=config('clash_api_config.default_url') . '/clans/%'.$tagId;
        return $this->getResponse($uri);
    }

    /**
     * desc:will return clan members by their ids
     * @param Request $request
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function clanMembers(GetClanMembersRequest $request)
    {
        $tagId = str_replace('#','',$request->tagId);
        $uri = config('clash_api_config.default_url') . '/clans/%'.$tagId.'/members';
        return $this->getResponse($uri);
    }

    /**
     * desc:will return warlog of the clan using its tag id .
     * @param Request $request
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function clanWarlog(ClanWarlogRequest $request)
    {
        $tagId = str_replace('#','',$request->tagId);
        $uri = config('clash_api_config.default_url') . '/clans/%'.$tagId.'/warlog';
        return $this->getResponse($uri);
    }

    /**
     * @param ClanCurrentWarRequest $request
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * desc:will return the current war of the clan using its tag id .
     */
    public function clanCurrentWar(ClanCurrentWarRequest $request)
    {
        $tagId = str_replace('#','',$request->tagId);
        $uri = config('clash_api_config.default_url') . '/clans/%'.$tagId.'/currentwar';
        return $this->getResponse($uri);
    }


    /**
     * @param GetPlayerByTagRequest $request
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * desc:will return the details of a player using his/her tag id
     */
    public function getPlayerByTag(GetPlayerByTagRequest $request)
    {
        $playerTag = str_replace('#','',$request->playerTag);
        $uri = config('clash_api_config.default_url') . '/players/%'.$playerTag;
        return $this->getResponse($uri);
    }


    /**
     * @param Request $request
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * desc:will return the list of all locations for the game
     */
    public function getLocations(Request $request)
    {
//        $locationId = str_replace('#','',$request->locationId);
        $uri = config('clash_api_config.default_url') . '/locations';
        return $this->getResponse($uri);
    }

    /**
     * @param GetLocationTopPlayers $request
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * desc:will return specific location top players by the id of the location
     */
    public function getLocationTopPlayers(GetLocationTopPlayers $request)
    {
        $locationId = str_replace('#','',$request->locationId);
        $uri = config('clash_api_config.default_url') . '/locations/'.$locationId.'/rankings/players';
        return $this->getResponse($uri);
    }

    /**
     * @param GetLocationTopClans $request
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * desc:will return the list of top clans for specific location
     */
    public function getLocationTopClans(GetLocationTopClans $request)
    {
        $locationId = str_replace('#','',$request->locationId);
        $uri = config('clash_api_config.default_url') . '/locations/'.$locationId.'/rankings/clans';
        return $this->getResponse($uri);
    }

    /**
     * desc:will send request to the end point using the guzzle object
     * @param $uri
     * @param array $params
     * @param string $method
     * @param null $headers
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getResponse($uri,array $params=[],$method='GET',$headers=null){
        try{
            $response = $this->guzzle->request($method, $uri , $params);
            return  $response->getBody()->__toString();
        }catch (\Exception $e){
            return [$e->getCode(),$e->getMessage()];
        }
    }

}
