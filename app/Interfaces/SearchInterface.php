<?php
/**
 * Created by PhpStorm.
 * User: NilGooN
 * Date: 30/07/2019
 * Time: 09:53 PM
 */

namespace App\Interfaces;


use App\Http\Requests\ClanCurrentWarRequest;
use App\Http\Requests\ClanSearchRequest;
use App\Http\Requests\ClanWarlogRequest;
use App\Http\Requests\GetClanMembersRequest;
use App\Http\Requests\GetLocationTopClans;
use App\Http\Requests\GetLocationTopPlayers;
use App\Http\Requests\GetPlayerByTagRequest;
use App\Http\Requests\SearchClanByTagRequest;
use Illuminate\Http\Request;

interface SearchInterface
{

    /**
     * @OA\Post(
     *     path="/search/clans",
     *     operationId="/clans",
     *     tags={"search clans"},
     *     @OA\Parameter(
     *         name="params",
     *         in="query",
     *         description="parameters for finding the specific clans params items: name , locationId ,minMembers,maxMembers,limit",
     *         required=true,
     *         @OA\Schema(type="string", default="{'name':'faramarz'}")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="and structure of the response is json",
     *     ),
     * )
     * @return mixed
     */
    public function clans(ClanSearchRequest $request);


    /**
     * @OA\Post(
     *     path="/search/clanByTag",
     *     operationId="/clanByTag",
     *     tags={"search clans"},
     *     @OA\Parameter(
     *         name="tagId",
     *         in="query",
     *         description="the tag if of the clan ",
     *         required=true,
     *         @OA\Schema(type="string", default="#20YU29YRR")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="and structure of the response is json",
     *     ),
     * )
     * @return mixed
     */
    public function clanByTag(SearchClanByTagRequest $request);




    /**
     * @OA\Post(
     *     path="/search/clanWarlog",
     *     operationId="/clanWarlog",
     *     tags={"search clans"},
     *     @OA\Parameter(
     *         name="tagId",
     *         in="query",
     *         description="the tag if of the clan ",
     *         required=true,
     *         @OA\Schema(type="string", default="#20YU29YRR")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will return the warlog details of the clan",
     *     ),
     * )
     * @return mixed
     */
    public function clanWarlog(ClanWarlogRequest $request);



    /**
     * @OA\Post(
     *     path="/search/clanMembers",
     *     operationId="/clanMembers",
     *     tags={"search clans"},
     *     @OA\Parameter(
     *         name="tagId",
     *         in="query",
     *         description="the tag if of the clan ",
     *         required=true,
     *         @OA\Schema(type="string", default="#20YU29YRR")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will return the members of the clan",
     *     ),
     * )
     * @return mixed
     */
    public function clanMembers(GetClanMembersRequest $request);


    /**
     * @OA\Post(
     *     path="/search/clanCurrentWar",
     *     operationId="/clanCurrentWar",
     *     tags={"search clans"},
     *     @OA\Parameter(
     *         name="tagId",
     *         in="query",
     *         description="the tag if of the clan ",
     *         required=true,
     *         @OA\Schema(type="string", default="#20YU29YRR")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will return the current war of the clan",
     *     ),
     * )
     * @return mixed
     */
    public function clanCurrentWar(ClanCurrentWarRequest $request);


    /**
     * @OA\Post(
     *     path="/search/player",
     *     operationId="/player",
     *     tags={"search players"},
     *     @OA\Parameter(
     *         name="playerTag",
     *         in="query",
     *         description="the tag id of a player ",
     *         required=true,
     *         @OA\Schema(type="string", default="#8C0VU8YY")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will return the details of a player using his or her tag id",
     *     ),
     * )
     * @return mixed
     */
    public function getPlayerByTag(GetPlayerByTagRequest $request);




    /**
     * @OA\Post(
     *     path="/search/locations",
     *     operationId="/locations",
     *     tags={"search locations"},
     *     @OA\Parameter(
     *         name="limit",
     *         in="query",
     *         description="the limit for the returned response",
     *         required=false,
     *         @OA\Schema(type="string", default="100")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will return the list of locations for a game",
     *     ),
     * )
     * @return mixed
     */
    public function getLocations(Request $request);


    /**
     * @OA\Post(
     *     path="/search/locationTopPlayers",
     *     operationId="/locationTopPlayers",
     *     tags={"search locations"},
     *     @OA\Parameter(
     *         name="locationId",
     *         in="query",
     *         description="id of the location",
     *         required=true,
     *         @OA\Schema(type="string", default="100")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will return the top players located in a specific location",
     *     ),
     * )
     * @return mixed
     */
    public function getLocationTopPlayers(GetLocationTopPlayers $request);

    /**
     * @OA\Post(
     *     path="/search/locationTopClans",
     *     operationId="/locationTopClans",
     *     tags={"search locations"},
     *     @OA\Parameter(
     *         name="locationId",
     *         in="query",
     *         description="id of the location",
     *         required=true,
     *         @OA\Schema(type="string", default="100")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="will return the top clans located in a specific location",
     *     ),
     * )
     * @return mixed
     */
    public function getLocationTopClans(GetLocationTopClans $request);

}