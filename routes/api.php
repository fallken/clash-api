<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/* Route::middleware(['first', 'second'])->group(function () {
    Route::get('/', function () {
        // Uses first & second Middleware
    });

    Route::get('user/profile', function () {
        // Uses first & second Middleware
    });
});
*/

Route::prefix('search')->group(function(){
    Route::post('/clans','SearchController@clans');
    Route::get('/test','SearchController@test');
    Route::post('/clanByTag','SearchController@clanByTag');
    Route::post('/clanMembers','SearchController@clanMembers');
    Route::post('/clanWarlog','SearchController@clanWarlog');
    Route::post('/clanCurrentWar','SearchController@clanCurrentWar');
    Route::post('/player','SearchController@getPlayerByTag');
    Route::post('/locations','SearchController@getLocations');
    Route::post('/locationTopPlayers','SearchController@getLocationTopPlayers');
    Route::post('/locationTopClans','SearchController@getLocationTopClans');
});